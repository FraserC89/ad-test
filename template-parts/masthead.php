<section class="cp-masthead h-outer-wrapper">
    <picture>
        <source srcset="/img/mastheads/masthead-1280x480.jpg" media="(min-width: 650px)">
        <img alt="" class="" srcset="img/mastheads/masthead-600x400.jpg">
    </picture>
    <div class="h-inner-wrapper">
        <div class="cp-masthead__copy">
            <h1 class="cp-masthead__title">Amplify your Talent</h1>
            <a href="/" class="cp-cta cp-cta--alt">Read More</a>
        </div>
    </div>
</section>