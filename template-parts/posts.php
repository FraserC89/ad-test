<div class="cp-posts__filter">
    <div class="">
        <button class="cp-cta">All</button>
        <button class="cp-cta">Read</button>
        <button class="cp-cta">Watch</button>
        <button class="cp-cta">Listen</button>
    </div>
    <div>
        <input type="text" placeholder="Search">
    </div>
</div>
<div class="cp-posts">

    <?php
        $json = file_get_contents('posts/posts.json');
        $posts = json_decode($json);

        foreach ($posts as $post) {
    ?>
        <?php
            $time = strtotime($post->date);
            $date = date('Y-m-d',$time);
        ?>
        <article class="cp-post" data-category="<?php echo $post->category?>">
            <div class="cp-post__wrapper">
                <img src="<?php echo $post->img; ?>" alt="<?php echo $post->imgAlt; ?>" class="cp-post__image" />

                <span class="cp-post__category cp-post--<?php echo $post->category?>">
                    <?php echo $post->category; ?>
                </span>

                <h1 class="cp-post__title">
                    <?php echo $post->title; ?>
                </h1>

                <time datetime="<?php echo $date; ?>">
                        <?php echo $post->date; ?>
                </time>

                <p class="cp-post__teaser">
                    <?php echo $post->teaser; ?>
                </p>
            </div>
            <a class="cp-cta" href="/">More Info</a>
        </article>

    <?php } ?>
</div>