<header class="cp-header">
    <div class="h-outer-wrapper">
        <div class="cp-header__logo">
            <img src="/css/img/logo.png" alt="logo">
        </div>
        <div class="cp-header__navs">
            <div>
                <nav class="cp-header__nav cp-header__primary-nav cp-primary-nav">
                    <ul>
                        <li>
                            <a class="cp-header__nav-link" href="/">What's On</a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link" href="/">Get Involved</a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link" href="/">Your Visit</a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link" href="/">Hire Us</a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link" href="/">Support Us</a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link" href="/">About Us</a>
                        </li>
                        <!-- <li>
                            <a class="cp-header__nav-link" href="/"><span class="h-screen-reader-text">Search</span></a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link" href="/"><span class="h-screen-reader-text">Login</span></a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link" href="/"><span class="h-screen-reader-text">Basket</span></a>
                        </li> -->
                    </ul>
                </nav>
                <nav class="cp-header__nav cp-header__secondary-nav cp-secondary-nav">
                    <ul>
                        <li>
                            <a class="cp-header__nav-link cp-header__nav-link--secondary" href="/">Accessibility</a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link cp-header__nav-link--secondary" href="/">Contact Us</a>
                        </li>
                        <li>
                            <a class="cp-header__nav-link cp-header__nav-link--secondary" href="/">News</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <nav class="cp-header__nav cp-header__icon-nav cp-icon-nav">
                <ul>
                    <li>
                        <a class="cp-header__nav-link cp-header__nav-icon" href="/"><svg class="lnr lnr-magnifier"><use xlink:href="#lnr-magnifier"></use></svg><span class="h-screen-reader-text">Search</span></a>
                    </li>
                    <li>
                        <a class="cp-header__nav-link cp-header__nav-icon" href="/"><svg class="lnr lnr-user"><use xlink:href="#lnr-user"></use></svg><span class="h-screen-reader-text">Login</span></a>
                    </li>
                    <li>
                        <a class="cp-header__nav-link cp-header__nav-icon" href="/"><svg class="lnr lnr-cart"><use xlink:href="#lnr-cart"></use></svg><span class="h-screen-reader-text">Basket</span></a>
                    </li>
                </ul>
            </nav>
        </div>
        <button class="cp-header__mobile-toggle">
            <span class="cp-header-burger"></span>
            <span class="h-screen-reader-text">Menu</span>
        </button>
    </div>
</header>