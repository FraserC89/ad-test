<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="dist/main.min.css">
    <link href="https://fonts.googleapis.com/css?family=Cabin:400,700|Open+Sans:400,700&display=swap" rel="stylesheet">
    <script src="https://cdn.linearicons.com/free/1.0.0/svgembedder.min.js"></script>
    <title>Nottingham Playhouse</title>
</head>
<body>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#content"></a>

        <?php include('template-parts/header.php'); ?>

        <?php include('template-parts/masthead.php'); ?>

        <main id="content" class="h-inner-wrapper cp-main-content">
            <?php include('template-parts/posts.php'); ?>
        </main>
        <?php include('template-parts/sign-up.php') ?>
        <footer class="cp-footer">
            <div class="h-inner-wrapper">
                <div class="cp-footer__contact">
                    <a class="cp-footer__contact-link" href="tel:01159419419">Box Office: 0115 941 9419</a>
                    <div class="cp-social-block">
                        <a class="cp-social-block__icon" href="/"><span class="h-screen-reader-text">Follow us on Facebook</span></a>
                        <a class="cp-social-block__icon" href="/"><span class="h-screen-reader-text">Follow us on SnapChat</span></a>
                        <a class="cp-social-block__icon" href="/"><span class="h-screen-reader-text">Follow us on YouTube</span></a>
                        <a class="cp-social-block__icon" href="/"><span class="h-screen-reader-text">Follow us on Instrgram</span></a>
                    </div>
                    <small>©2018 Nottingham Playhouse. Registered Charity 1109342. Nottingham Playhouse, Wellington Circus, Nottingham, NG1 5AF</small>
                </div>
                <nav class="cp-footer__nav">
                    <ul>
                        <li>
                            <a class="cp-footer__nav-link" href="/">What's On</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Get Involved</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Your Visit</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Hire Us</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Support Us</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">About Us</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">WNews</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Accessibility</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Contact Us</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Careers</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Memberships</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Press</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Privacy Policy</a>
                        </li>
                        <li>
                            <a class="cp-footer__nav-link" href="/">Terms & Conditions</a>
                        </li>
                    </ul>
                </nav>
                <div class="cp-footer__logos">
                    <img class="cp-footer__logo" src="/css/img/arts-council-logo.png" alt="Arts Council England">
                    <img class="cp-footer__logo" src="/css/img/nottingham-city-council.png" alt="Nottingham City Council">
                </div>
            </div>
        </footer>
    </div>
</body>
</html>